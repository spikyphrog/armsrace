It's a 3rd person shooter, multiplayer game. The idea behind it was to be a Gun Game match, whereby killing your opponents you get points and when you get enough points, you "level" up your weapon by changing the weapons.

I barely made it in time for the showcase, however, due to me setting up for the first time multiplayer game in Unreal, I could not make it work during the showcase but once I got home I managed to get two pc on the same network connected - [here](https://youtu.be/boWV7-gZw48)

This game jam was the perfect opportunity for me to sit down and do something on my own that I have not done before. 

I've done the coding and I had some help for the UI that could be seen in the game by Silvia Gancheva.

Link to the game can be found [here](https://spikyphrog.itch.io/arms-race)