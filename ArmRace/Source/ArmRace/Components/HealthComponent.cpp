// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"

#include "ArmRace/GameModes/MainGameMode.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.bStartWithTickEnabled = false;

	MaxHealth = 100.f;
	bHasAwardedPoint = false;
	
	SetIsReplicatedByDefault(true);
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{

	Super::BeginPlay();

	if (GetOwnerRole() == ROLE_Authority)
	{
		AActor* ComponentOwner = GetOwner();

		if (ComponentOwner)
		{
			ComponentOwner->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::HandleDamage);
		}
	}

	CurrentHealth = MaxHealth;
}

void UHealthComponent::HandleDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage<= 0.f)
	{
		return;
	}
	
	CurrentHealth = FMath::Clamp(CurrentHealth - Damage, 0.f, MaxHealth);
	
	OnHealthChanged.Broadcast(this, CurrentHealth, Damage, DamageType, InstigatedBy, DamageCauser);

	if (CurrentHealth <= 0.f)
	{
		AMainGameMode* GameMode = Cast<AMainGameMode>(GetWorld()->GetAuthGameMode());

		if (GameMode && !bHasAwardedPoint)
		{
			GameMode->OnPawnKill.Broadcast(GetOwner(), DamageCauser, InstigatedBy);
			bHasAwardedPoint = true;

		}	
	}
}

float UHealthComponent::GetHealth() const
{
	return CurrentHealth;
}

void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UHealthComponent, CurrentHealth);
}

