// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "PowerUp.generated.h"

UCLASS()
class ARMRACE_API APowerUp : public AActor
{
	GENERATED_BODY()
private:
	UPROPERTY(VisibleAnywhere)
	class UHealthComponent* HealthComponent;

	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* StaticMeshComponent;
	
	UPROPERTY(EditDefaultsOnly)
	UParticleSystem* ExplosionEffect;

	UPROPERTY(EditDefaultsOnly)
	URadialForceComponent* RadialForceComponent;

	UPROPERTY(EditDefaultsOnly)
	float ExplosionImpulse;
public:
	UPROPERTY(ReplicatedUsing=OnRep_Exploded, VisibleAnywhere)
	bool bDied;

	UFUNCTION()
	void OnRep_Exploded();
	// Sets default values for this actor's properties
	APowerUp();

private:
	UFUNCTION()
	void OnHealthChanged(UHealthComponent* HealthComp, float CurrentHealth, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

};
