// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerUp.h"
#include "ArmRace/Components/HealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"


// Sets default values
APowerUp::APowerUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
	HealthComponent->OnHealthChanged.AddDynamic(this, &APowerUp::OnHealthChanged);
	
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static mesh"));
	StaticMeshComponent->SetSimulatePhysics(true);
	StaticMeshComponent->SetCollisionObjectType(ECC_PhysicsBody);
	RootComponent = StaticMeshComponent;

	RadialForceComponent = CreateDefaultSubobject<URadialForceComponent>(TEXT("Radial force"));
	RadialForceComponent->SetupAttachment(RootComponent);
	RadialForceComponent->Radius = 250;
	RadialForceComponent->bImpulseVelChange = true;
	RadialForceComponent->bAutoActivate = false;
	RadialForceComponent->bIgnoreOwningActor = true;
	
	ExplosionImpulse = 500.f;

	SetReplicates(true);
}

void APowerUp::OnHealthChanged(UHealthComponent* HealthComp, float CurrentHealth, float Damage,
	const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (CurrentHealth <= 0.f && !bDied)
	{
		// Die
		bDied = true;
		OnRep_Exploded();
		
		FVector BoostIntensity = FVector::UpVector * ExplosionImpulse;
		StaticMeshComponent->AddImpulse(BoostIntensity, NAME_None, true);
		
		RadialForceComponent -> FireImpulse();

		SetLifeSpan(10.f);
	}
}

void APowerUp::OnRep_Exploded()
{
	if (ExplosionEffect)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorLocation(), GetActorRotation());
	}
}

void APowerUp::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(APowerUp, bDied);
}




