// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Weapon.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReload, bool, IsReloading);


USTRUCT()
struct FHitScanTrace
{
	GENERATED_BODY()

	UPROPERTY()
	TEnumAsByte<EPhysicalSurface> SurfaceType;

	UPROPERTY()
	FVector_NetQuantize TraceTo;	
};

UCLASS()
class ARMRACE_API AWeapon : public AActor
{
	GENERATED_BODY()
protected:
	UPROPERTY(EditDefaultsOnly)
	USkeletalMeshComponent* MeshComponent;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UDamageType> DamageTypeClass;
	
	UPROPERTY(EditDefaultsOnly, Category="Weapon Base")
	UParticleSystem* MuzzleEffect;
	
	UPROPERTY(EditDefaultsOnly, Category="Weapon Base")
	UParticleSystem* DefaultImpactEffect;

	UPROPERTY(EditDefaultsOnly, Category="Weapon Base")
	UParticleSystem* FleshImpactEffect;

	UPROPERTY(EditDefaultsOnly, Category="Weapon Base")
	float BaseDamage = 20.f;

	UPROPERTY(EditDefaultsOnly, Category="Weapon Base")
	float RateOfFire;

	UPROPERTY(EditDefaultsOnly, Category="Weapon Base")
	float TimeToReload;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Weapon Base")
	int32 MaxClipCapacity;

	UPROPERTY(Replicated, BlueprintReadOnly)
	int32 CurrentClipCapacity;

	UPROPERTY(VisibleAnywhere)
	FName MuzzleSocket;
	
	UPROPERTY()
	FTimerHandle TimerHandle_TimeBetweenShots;

	UPROPERTY()
	FTimerHandle TimerHandle_TimeToReload;
	
	UPROPERTY()
	float TimeBetweenShots;

	UPROPERTY()
	float LastTimeShot;
	
	virtual void BeginPlay() override;

	UPROPERTY(ReplicatedUsing=OnRep_HitScanTrace)
	FHitScanTrace HitScanTrace;
	
	UFUNCTION()
	void OnRep_HitScanTrace();

	
public:
	UPROPERTY(BlueprintAssignable, Category="Events")
	FOnWeaponReload OnReload;
	
	UPROPERTY()
	bool bIsReloading;
	
	// Sets default values for this actor's properties
	AWeapon();
	
	void StartFire();

	void StopFire();

	void StartReload();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerFire();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerReload();

private:
	void FireParticleFX() const;
	void FireImpactFX(EPhysicalSurface SurfaceType, FVector ImpactPoint);
	
	void Fire();
	void Reload();

	UFUNCTION()
	void CheckReloading(bool IsReloading);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

};
