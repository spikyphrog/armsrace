// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"
#include "ArmRace/ArmRace.h"
#include "DrawDebugHelpers.h"
#include "ArmRace/Characters/PlayerCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"


// Sets default values
AWeapon::AWeapon()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	MeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh comp"));
	RootComponent = MeshComponent;

	MuzzleSocket = "Muzzle";
	RateOfFire = 300.f;
	MaxClipCapacity = 30;
	TimeToReload = 2.f;
	bIsReloading = false;

	SetReplicates(true);
	NetUpdateFrequency = 66.f;
	MinNetUpdateFrequency = 33.f;
	
}

void AWeapon::BeginPlay()
{
	Super::BeginPlay();
	TimeBetweenShots = 60 / RateOfFire;
	CurrentClipCapacity = MaxClipCapacity;

	OnReload.AddDynamic(this, &AWeapon::CheckReloading);
}


void AWeapon::StartFire()
{
	const float FirstDelay = FMath::Max(LastTimeShot + TimeBetweenShots - GetWorld()->TimeSeconds, 0.f);
	
	GetWorldTimerManager().SetTimer(TimerHandle_TimeBetweenShots, this, &AWeapon::Fire, TimeBetweenShots, true, FirstDelay);
}

void AWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_TimeBetweenShots);
}

void AWeapon::StartReload()
{
	if (GetLocalRole() < ROLE_Authority)
	{
		ServerReload();
	}
	
	if (CurrentClipCapacity != MaxClipCapacity)
	{
		bIsReloading = true;
		
		OnReload.Broadcast(bIsReloading);
		
		GetWorldTimerManager().SetTimer(TimerHandle_TimeToReload, this, &AWeapon::Reload, TimeToReload, false);
	}
}


void AWeapon::Fire()
{
	if (GetLocalRole() < ROLE_Authority)
	{
		ServerFire();
	}
	// Trace the world from pawn eyes to crosshair
	AActor* WeaponOwner = GetOwner();

	if (WeaponOwner && CurrentClipCapacity > 0 && !bIsReloading)
	{
		FVector EyeLocation;
		FRotator EyeRotation;
		WeaponOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);

		FVector ShotDirection = EyeRotation.Vector();
		
		FVector TraceEnd = EyeLocation + (ShotDirection * 10000);
		FHitResult HitResults;

		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(WeaponOwner);
		QueryParams.AddIgnoredActor(this);
		QueryParams.bTraceComplex = true;
		QueryParams.bReturnPhysicalMaterial = true;

		EPhysicalSurface SurfaceType = SurfaceType_Default;
		
		if(GetWorld()->LineTraceSingleByChannel(HitResults, EyeLocation, TraceEnd, COLLISION_WEAPON, QueryParams))
		{
			// block hit, process dmg
			AActor* HitActor = HitResults.GetActor();

			SurfaceType = UPhysicalMaterial::DetermineSurfaceType(HitResults.PhysMaterial.Get());

			float Damage = BaseDamage;
			if (SurfaceType == SURFACE_FLESHWEAKNESS)
			{
				Damage *= 4.f;
			}

			UGameplayStatics::ApplyPointDamage(HitActor, Damage, ShotDirection, HitResults, WeaponOwner->GetInstigatorController(), this, DamageTypeClass);

			FireImpactFX(SurfaceType, HitResults.ImpactPoint);
			
			FireParticleFX();
			
			CurrentClipCapacity --;
		}
		
		if (GetLocalRole() == ROLE_Authority)
		{
			HitScanTrace.TraceTo = TraceEnd;
			HitScanTrace.SurfaceType = SurfaceType;
		}
		
		LastTimeShot = GetWorld()->TimeSeconds;
	}
}


void AWeapon::OnRep_HitScanTrace()
{
	// Play cosmetic FX
	FireParticleFX();
	FireImpactFX(HitScanTrace.SurfaceType, HitScanTrace.TraceTo);
}


void AWeapon::ServerFire_Implementation()
{
	Fire();
}

bool AWeapon::ServerFire_Validate()
{
	return true;
}


void AWeapon::Reload()
{
	CurrentClipCapacity = MaxClipCapacity;
	bIsReloading = false;
	
	OnReload.Broadcast(bIsReloading);
}

void AWeapon::CheckReloading(bool IsReloading)
{
	Cast<APlayerCharacter>(GetOwner())->bReloading = IsReloading;
}

void AWeapon::ServerReload_Implementation()
{
	Reload();
}

bool AWeapon::ServerReload_Validate()
{
	return true;
}

void AWeapon::FireParticleFX() const
{
	if (MuzzleEffect)
	{
		UGameplayStatics::SpawnEmitterAttached(MuzzleEffect, MeshComponent, MuzzleSocket);
	}
}

void AWeapon::FireImpactFX(EPhysicalSurface SurfaceType, FVector ImpactPoint)
{
	UParticleSystem* SelectedEffect = nullptr;
	switch (SurfaceType)
	{
	case SURFACE_FLESHDEFAULT:
	case SURFACE_FLESHWEAKNESS:
		SelectedEffect = FleshImpactEffect;
		break;

	default:
		SelectedEffect = DefaultImpactEffect;
		break;
	}
			
	if (SelectedEffect)
	{
		FVector ShotDirection = ImpactPoint - MeshComponent->GetSocketLocation(MuzzleSocket);
		ShotDirection.Normalize();
		
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SelectedEffect, ImpactPoint, ShotDirection.Rotation());
	}
}


void AWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AWeapon, HitScanTrace, COND_SkipOwner);
	DOREPLIFETIME(AWeapon, CurrentClipCapacity);
}

