// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerCharacter.h"


#include "ArmRace/ArmRace.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "ArmRace/GameModes/MainGameMode.h"
#include "ArmRace/Actors/Weapon.h"
#include "ArmRace/Components/HealthComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Net/UnrealNetwork.h"


// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	// Setting up the spring arm
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm")); // Creating the spring arm
	SpringArm->SetupAttachment(RootComponent); // Attaching the spring arm to the root component
	SpringArm->bUsePawnControlRotation = true; // Enabling pawn control rotation

	// Setting up the camera
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera Component"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);

	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true;
	
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

	
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
	HealthComponent->OnHealthChanged.AddDynamic(this, &APlayerCharacter::OnHealthChanged);
	
	
	ZoomedFOV = 65.f;
	TimerDeltaTime = 0.01f;
	ZoomInterpolationSpeed = 1.f;
	WeaponSocketName = "WeaponSocket";
	bDied = false;
	bReloading = false;
	CurrentLevel = 0;
}


// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	DefaultFOV = Camera->FieldOfView;
	GetWorldTimerManager().SetTimer(ZoomCheckTimerHandle, this, &APlayerCharacter::CheckZoomIn, TimerDeltaTime, true);
	
	LoadWeapons();

	MaxLevel = PooledWeapons.Num();
	
	if (HasAuthority())
	{
		SpawnWeapon(CurrentLevel);
	}
	
}


// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent-> BindAxis(TEXT("MoveForward"), this, &APlayerCharacter::MoveForward);
	PlayerInputComponent-> BindAxis(TEXT("MoveRight"), this, &APlayerCharacter::MoveRight);

	PlayerInputComponent->BindAxis(TEXT("Turn"), this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis(TEXT("TurnRate"), this, &APlayerCharacter::TurnRate);

	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this,&APlayerCharacter::LookUpRate);
	
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Released, this, &ACharacter::StopJumping);
	
	PlayerInputComponent->BindAction(TEXT("Crouch"), IE_Pressed, this, &APlayerCharacter::PlayerCrouch);
	PlayerInputComponent->BindAction(TEXT("Crouch"), IE_Released, this, &APlayerCharacter::StandUp);

	PlayerInputComponent->BindAction(TEXT("ZoomIn"), IE_Pressed, this, &APlayerCharacter::ZoomIn);
	PlayerInputComponent->BindAction(TEXT("ZoomIn"), IE_Released, this, &APlayerCharacter::ZoomOut);

	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &APlayerCharacter::StartFire);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Released, this, &APlayerCharacter::StopFire);
	
	PlayerInputComponent->BindAction(TEXT("Reload"), IE_Pressed, this, &APlayerCharacter::Reload);
}

void APlayerCharacter::MoveForward(float Value)
{
	/*const FRotator Rotation = Controller -> GetControlRotation(); // Getting the rotation of the character
	const FRotator Yaw (0.f, Rotation.Yaw, 0.f); // Getting the forward Vector of the rotation
	const FVector Direction = FRotationMatrix(Yaw).GetUnitAxis(EAxis::X); // X axis is the forward direction in Unreal*/
	AddMovementInput(GetActorForwardVector(), Value);
}

void APlayerCharacter::MoveRight(float Value)
{
	/*const FRotator Rotation = Controller -> GetControlRotation(); // Getting the rotation of the character
	const FRotator Yaw (0.f, Rotation.Yaw, 0.f); // Getting the forward Vector of the rotation
	const FVector Direction = FRotationMatrix(Yaw).GetUnitAxis(EAxis::Y); // Y axis is for the left and right direction in Unreal*/
	AddMovementInput(GetActorRightVector(), Value);
}

void APlayerCharacter::TurnRate(float Value)
{
	AddControllerYawInput(Value * GetWorld()->GetDeltaSeconds() * TurnRotation);
}

void APlayerCharacter::LookUpRate(float Value)
{
	AddControllerPitchInput(Value * GetWorld()->GetDeltaSeconds() * LookRate);
}

void APlayerCharacter::PlayerCrouch()
{
	Crouch(true);
}

void APlayerCharacter::StandUp()
{
	UnCrouch(true);
}

FVector APlayerCharacter::GetPawnViewLocation() const
{
	if(Camera)
		return Camera->GetComponentLocation();

	return Super::GetPawnViewLocation();
}

void APlayerCharacter::CheckZoomIn()
{
	const float TargetFOV = bZoomIn ? ZoomedFOV : DefaultFOV;

	const float NewFOV = FMath::FInterpTo(Camera->FieldOfView, TargetFOV, TimerDeltaTime, ZoomInterpolationSpeed);
	
	Camera->SetFieldOfView(NewFOV);
}

void APlayerCharacter::ZoomIn()
{
	bZoomIn = true;
}

void APlayerCharacter::ZoomOut()
{
	bZoomIn = false;
}

void APlayerCharacter::Reload()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StartReload();
	}
}

void APlayerCharacter::StartFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StartFire();
	}
}

void APlayerCharacter::StopFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
	}
}

void APlayerCharacter::SpawnWeapon(int32 WeaponIndex)
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	CurrentWeapon = GetWorld()->SpawnActor<AWeapon>(PooledWeapons[WeaponIndex], FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
	
	if (CurrentWeapon)
	{
		CurrentWeapon->SetOwner(this);
		CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponSocketName);
	}
}

void APlayerCharacter::OnHealthChanged(UHealthComponent* HealthComp, float CurrentHealth, float Damage,
	const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (CurrentHealth <= 0.f && !bDied)
	{
		// Die
		bDied = true;

		AMainGameMode* GameMode = Cast<AMainGameMode>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			GameMode->OnPlayerKilled.Broadcast();
		}
		
		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		DetachFromControllerPendingDestroy();

		SetLifeSpan(4.f);
	}
}

void APlayerCharacter::LoadWeapons()
{
	PooledWeapons.Add(AKWeapon);
	PooledWeapons.Add(AKUWeapon);
	PooledWeapons.Add(ARWeapon);
	PooledWeapons.Add(ARUWeapon);
}

void APlayerCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APlayerCharacter, CurrentWeapon);
	DOREPLIFETIME(APlayerCharacter, bDied);
	DOREPLIFETIME(APlayerCharacter, PooledWeapons);
	DOREPLIFETIME(APlayerCharacter, CurrentLevel);
	DOREPLIFETIME(APlayerCharacter, MaxLevel);
}


