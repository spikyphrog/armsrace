// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ArmRace/GameModes/MainPlayerState.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

class AWeapon;

UCLASS()
class ARMRACE_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()
	
	// Variables
private:
	UPROPERTY(EditDefaultsOnly)
	class UCameraComponent* Camera; // Declaring a camera

	UPROPERTY(EditDefaultsOnly)
	class USpringArmComponent* SpringArm; // Declaring a spring arm

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	class UHealthComponent* HealthComponent;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float TurnRotation = 45.f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float LookRate = 45.f;

	UPROPERTY(VisibleAnywhere)
	bool bZoomIn;

	UPROPERTY(EditDefaultsOnly)
	float ZoomedFOV;

	UPROPERTY(VisibleAnywhere)
	float DefaultFOV;

	UPROPERTY()
	FTimerHandle ZoomCheckTimerHandle;

	UPROPERTY()
	float TimerDeltaTime;

	UPROPERTY(EditDefaultsOnly, meta = (ClampMin = 0.1, ClampMax = 100))
	float ZoomInterpolationSpeed;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess = "true"), Replicated)
	AWeapon* CurrentWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"), Replicated)
	TArray<TSubclassOf<AWeapon>> PooledWeapons;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWeapon> AKWeapon;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWeapon> AKUWeapon;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWeapon> ARWeapon;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWeapon> ARUWeapon;

	UPROPERTY(EditDefaultsOnly)
	FName WeaponSocketName;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	bool bDied; // Has the played died?

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bReloading;

	UPROPERTY(BlueprintReadOnly, Replicated)
	int32 MaxLevel;
	
	UPROPERTY(BlueprintReadWrite, Replicated)
	int32 CurrentLevel;

	// Sets default values for this character's properties
	APlayerCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	//  ==== Input functions ==== 
	UFUNCTION()
	void MoveForward(float Value);
	
	UFUNCTION()
	void MoveRight(float Value);
	
	UFUNCTION()
	void TurnRate(float Value);

	UFUNCTION()
	void LookUpRate(float Value);

	UFUNCTION()
	void PlayerCrouch();

	UFUNCTION()
	void StandUp();
	
	UFUNCTION()
	void StartFire();
	
	UFUNCTION()
	void StopFire();
	
	UFUNCTION()
	void ZoomIn();

	UFUNCTION()
	void ZoomOut();

	UFUNCTION()
	void Reload();
	// ============================
	
	// Getting the eyes of the pawn, overriden to trace from the camera
	virtual FVector GetPawnViewLocation() const override;

	// Function bound to the timer to check every mil sec whether the player zooms in or not
	void CheckZoomIn();

	// Function to spawn the player's weapon based on the TSubclass<AWeapon>
	UFUNCTION(BlueprintCallable)
	void SpawnWeapon(int32 WeaponIndex);

	UFUNCTION()
	void OnHealthChanged(UHealthComponent* HealthComp, float CurrentHealth, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);

	void LoadWeapons();
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
};
