// Fill out your copyright notice in the Description page of Project Settings.


#include "MainGameState.h"

#include "Net/UnrealNetwork.h"

void AMainGameState::SetWaveState(EWaveState NewState)
{
	if (HasAuthority())
	{
		const EWaveState OldState = WaveState;
		
		WaveState = NewState;
		OnRep_WaveState(OldState);
	}
}

void AMainGameState::OnRep_WaveState(EWaveState OldState)
{
	WaveStateChanged(WaveState, OldState);
}

void AMainGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AMainGameState, WaveState);
}
