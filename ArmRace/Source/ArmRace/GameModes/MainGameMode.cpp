// Fill out your copyright notice in the Description page of Project Settings.


#include "MainGameMode.h"

#include "EngineUtils.h"
#include "ArmRace/Components/HealthComponent.h"
#include "Kismet/GameplayStatics.h"

AMainGameMode::AMainGameMode()
{
	GameStateClass = AMainGameState::StaticClass();
	PlayerStateClass = AMainPlayerState::StaticClass();
	
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickInterval = 1.f;
	
	TimeBetweenWaves = 2.f;
	bIsPaused = false;
}

void AMainGameMode::BeginPlay()
{
	Super::BeginPlay();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(),PlayerCharacterClass, PlayersSpawned);
	
	PrepareForNextWave();
	OnPlayerKilled.AddDynamic(this, &AMainGameMode::RespawnDeadPlayers);
}

void AMainGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	CheckWaveState();
	CheckPlayersAlive();
}

void AMainGameMode::StartWave()
{
	if (CurrentWave <= MaxWaves)
	{
		CurrentWave++;

		AmountBotsToSpawn = 2 * (CurrentWave + PlayersSpawned.Num());
		
		GetWorldTimerManager().SetTimer(TimerHandle_BotSpawner, this, &AMainGameMode::SpawnBotElapsedTimer, 1.f, true, 0.f); 

		SetWaveState(EWaveState::WaveInProgress);
	}
}

void AMainGameMode::EndWave()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_BotSpawner);
	
	SetWaveState(EWaveState::WaitingToComplete);
}

void AMainGameMode::PrepareForNextWave()
{
	GetWorldTimerManager().SetTimer(TimerHandle_NextWaveStart, this, &AMainGameMode::StartWave, TimeBetweenWaves,false);
	SetWaveState(EWaveState::WaitingToStart);
}

void AMainGameMode::CheckWaveState()
{
	const bool bIsPreparingForWave = GetWorldTimerManager().IsTimerActive(TimerHandle_NextWaveStart);
	bool bAreBotsAlive = false;
	
	if (AmountBotsToSpawn > 0 || bIsPreparingForWave)
	{
		return;
	}
	
	for (TActorIterator<AActor> Iterator (GetWorld(), EnemyCharacterClass); Iterator; ++Iterator)
	{
		APawn* EnemyPawn = Iterator->GetInstigator();
		
		if (EnemyPawn == nullptr || EnemyPawn->IsPlayerControlled())
		{
			continue;
		}
		
		UHealthComponent* EnemyHealthComponent = Cast<UHealthComponent>(EnemyPawn->GetComponentByClass(UHealthComponent::StaticClass()));

		if (EnemyHealthComponent && EnemyHealthComponent->GetHealth() > 0.f)
		{
			bAreBotsAlive = true;
			break;
		}
	}

	if (!bAreBotsAlive)
	{
		SetWaveState(EWaveState::WaveComplete);
		
		PrepareForNextWave();
	}
}

void AMainGameMode::CheckPlayersAlive()
{
	for(FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		APlayerController* PlayerController = Iterator->Get();

		if (PlayerController && PlayerController->GetPawn())
		{
			const APawn* PlayerPawn = PlayerController->GetPawn();

			UHealthComponent* PlayerHealthComponent = Cast<UHealthComponent>(PlayerPawn->GetComponentByClass(UHealthComponent::StaticClass()));

			if (PlayerHealthComponent && PlayerHealthComponent->GetHealth() > 0.f)
			{
				// Players are alive
				return;
			}
		}
	}
	// no players alive
	GameOver();
}

void AMainGameMode::SetWaveState(EWaveState NewState) const
{
	AMainGameState* GS = GetGameState<AMainGameState>();

	if (GS)
	{
		GS->SetWaveState(NewState);
	}
}

void AMainGameMode::RespawnDeadPlayers()
{
	FTimerHandle TimerHandle_RespawnDelay;

	GetWorldTimerManager().SetTimer(TimerHandle_RespawnDelay, this, &AMainGameMode::Respawn, 0.1f, false, 3.f);
}

void AMainGameMode::Respawn()
{
	for(FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		APlayerController* PlayerController = Iterator->Get();

		if (PlayerController && PlayerController->GetPawn() == nullptr)
		{
			RestartPlayer(PlayerController);
		}
	}
}


void AMainGameMode::GameOver()
{
	EndWave();
	SetWaveState(EWaveState::GameOver);
}

void AMainGameMode::SpawnBotElapsedTimer()
{
	SpawnBot();
	AmountBotsToSpawn--;

	if (AmountBotsToSpawn <= 0)
	{
		EndWave();
	}
}
