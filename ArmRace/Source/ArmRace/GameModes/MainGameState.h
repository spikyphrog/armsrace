// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "MainGameState.generated.h"

UENUM(BlueprintType)
enum class EWaveState : uint8
{
	WaitingToStart,
	
	PreparingNextWave,

	WaveInProgress,

	// Waiting for players to complete the wave
	WaitingToComplete,

	WaveComplete,

	GameOver
};

/**
 * 
 */
UCLASS()
class ARMRACE_API AMainGameState : public AGameStateBase
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadOnly, ReplicatedUsing=OnRep_WaveState)
	EWaveState WaveState;

	UFUNCTION()
	void SetWaveState(EWaveState NewState);

protected:
	UFUNCTION(BlueprintImplementableEvent)
	void WaveStateChanged(EWaveState NewState, EWaveState OldState);

	UFUNCTION()
	void OnRep_WaveState(EWaveState OldState);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
};
