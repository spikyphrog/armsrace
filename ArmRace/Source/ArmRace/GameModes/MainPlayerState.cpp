// Fill out your copyright notice in the Description page of Project Settings.


#include "MainPlayerState.h"

void AMainPlayerState::AddScore(float ScoreToReward)
{
	const float NewScore = GetScore() + ScoreToReward;
	
	SetScore(NewScore);
}
