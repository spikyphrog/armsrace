// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MainGameState.h"
#include "MainPlayerState.h"
#include "GameFramework/GameModeBase.h"
#include "MainGameMode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnPawnKill, AActor*, VictimActor, AActor*, KillerActor, AController*, KillerController);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPlayerKilled);
/**
 * 
 */
UCLASS()
class ARMRACE_API AMainGameMode : public AGameModeBase
{
	GENERATED_BODY()

private:
	FTimerHandle TimerHandle_BotSpawner;
	FTimerHandle TimerHandle_NextWaveStart;
	FTimerHandle TimerHandle_CheckWaveState;

	UPROPERTY(VisibleAnywhere)
	TArray<AActor*> PlayersSpawned;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AActor> PlayerCharacterClass;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AActor> EnemyCharacterClass;
	
	UPROPERTY(EditDefaultsOnly)
	int32 AmountBotsToSpawn;
	
	UPROPERTY(EditDefaultsOnly)
	int32 MaxWaves;
	
	UPROPERTY(VisibleAnywhere)
	int32 CurrentWave;

	UPROPERTY(EditDefaultsOnly)
	float TimeBetweenWaves;

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool bIsPaused;
	
	AMainGameMode();

	UPROPERTY(BlueprintAssignable)
	FOnPawnKill OnPawnKill;

	UPROPERTY()
	FOnPlayerKilled OnPlayerKilled;
	
protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;
	
	UFUNCTION(BlueprintImplementableEvent, Category="Gamemode")
	void SpawnBot();

	UFUNCTION()
	void SpawnBotElapsedTimer();
	
	UFUNCTION()
	void StartWave();

	UFUNCTION()
	void EndWave();

	UFUNCTION(BlueprintCallable)
	void GameOver();
	
	UFUNCTION()
	void PrepareForNextWave();

	UFUNCTION()
	void CheckWaveState();

	UFUNCTION()
	void CheckPlayersAlive();
	
	void SetWaveState(EWaveState NewState) const;

	UFUNCTION()
	void RespawnDeadPlayers();

	UFUNCTION()
	void Respawn();

};
