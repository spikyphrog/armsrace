// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ArmRace/GameModes/MainGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMainGameMode() {}
// Cross Module References
	ARMRACE_API UFunction* Z_Construct_UDelegateFunction_ArmRace_OnPlayerKilled__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_ArmRace();
	ARMRACE_API UFunction* Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AController_NoRegister();
	ARMRACE_API UClass* Z_Construct_UClass_AMainGameMode_NoRegister();
	ARMRACE_API UClass* Z_Construct_UClass_AMainGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_ArmRace_OnPlayerKilled__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_ArmRace_OnPlayerKilled__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_ArmRace_OnPlayerKilled__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_ArmRace, nullptr, "OnPlayerKilled__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_ArmRace_OnPlayerKilled__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_ArmRace_OnPlayerKilled__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_ArmRace_OnPlayerKilled__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_ArmRace_OnPlayerKilled__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature_Statics
	{
		struct _Script_ArmRace_eventOnPawnKill_Parms
		{
			AActor* VictimActor;
			AActor* KillerActor;
			AController* KillerController;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VictimActor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_KillerActor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_KillerController;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature_Statics::NewProp_VictimActor = { "VictimActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_ArmRace_eventOnPawnKill_Parms, VictimActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature_Statics::NewProp_KillerActor = { "KillerActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_ArmRace_eventOnPawnKill_Parms, KillerActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature_Statics::NewProp_KillerController = { "KillerController", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_ArmRace_eventOnPawnKill_Parms, KillerController), Z_Construct_UClass_AController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature_Statics::NewProp_VictimActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature_Statics::NewProp_KillerActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature_Statics::NewProp_KillerController,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_ArmRace, nullptr, "OnPawnKill__DelegateSignature", nullptr, nullptr, sizeof(_Script_ArmRace_eventOnPawnKill_Parms), Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(AMainGameMode::execRespawn)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Respawn();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMainGameMode::execRespawnDeadPlayers)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RespawnDeadPlayers();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMainGameMode::execCheckPlayersAlive)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CheckPlayersAlive();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMainGameMode::execCheckWaveState)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CheckWaveState();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMainGameMode::execPrepareForNextWave)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->PrepareForNextWave();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMainGameMode::execGameOver)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GameOver();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMainGameMode::execEndWave)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EndWave();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMainGameMode::execStartWave)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StartWave();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMainGameMode::execSpawnBotElapsedTimer)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SpawnBotElapsedTimer();
		P_NATIVE_END;
	}
	static FName NAME_AMainGameMode_SpawnBot = FName(TEXT("SpawnBot"));
	void AMainGameMode::SpawnBot()
	{
		ProcessEvent(FindFunctionChecked(NAME_AMainGameMode_SpawnBot),NULL);
	}
	void AMainGameMode::StaticRegisterNativesAMainGameMode()
	{
		UClass* Class = AMainGameMode::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CheckPlayersAlive", &AMainGameMode::execCheckPlayersAlive },
			{ "CheckWaveState", &AMainGameMode::execCheckWaveState },
			{ "EndWave", &AMainGameMode::execEndWave },
			{ "GameOver", &AMainGameMode::execGameOver },
			{ "PrepareForNextWave", &AMainGameMode::execPrepareForNextWave },
			{ "Respawn", &AMainGameMode::execRespawn },
			{ "RespawnDeadPlayers", &AMainGameMode::execRespawnDeadPlayers },
			{ "SpawnBotElapsedTimer", &AMainGameMode::execSpawnBotElapsedTimer },
			{ "StartWave", &AMainGameMode::execStartWave },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AMainGameMode_CheckPlayersAlive_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainGameMode_CheckPlayersAlive_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainGameMode_CheckPlayersAlive_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainGameMode, nullptr, "CheckPlayersAlive", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainGameMode_CheckPlayersAlive_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMainGameMode_CheckPlayersAlive_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainGameMode_CheckPlayersAlive()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainGameMode_CheckPlayersAlive_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMainGameMode_CheckWaveState_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainGameMode_CheckWaveState_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainGameMode_CheckWaveState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainGameMode, nullptr, "CheckWaveState", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainGameMode_CheckWaveState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMainGameMode_CheckWaveState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainGameMode_CheckWaveState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainGameMode_CheckWaveState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMainGameMode_EndWave_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainGameMode_EndWave_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainGameMode_EndWave_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainGameMode, nullptr, "EndWave", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainGameMode_EndWave_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMainGameMode_EndWave_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainGameMode_EndWave()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainGameMode_EndWave_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMainGameMode_GameOver_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainGameMode_GameOver_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainGameMode_GameOver_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainGameMode, nullptr, "GameOver", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainGameMode_GameOver_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMainGameMode_GameOver_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainGameMode_GameOver()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainGameMode_GameOver_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMainGameMode_PrepareForNextWave_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainGameMode_PrepareForNextWave_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainGameMode_PrepareForNextWave_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainGameMode, nullptr, "PrepareForNextWave", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainGameMode_PrepareForNextWave_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMainGameMode_PrepareForNextWave_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainGameMode_PrepareForNextWave()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainGameMode_PrepareForNextWave_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMainGameMode_Respawn_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainGameMode_Respawn_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainGameMode_Respawn_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainGameMode, nullptr, "Respawn", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainGameMode_Respawn_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMainGameMode_Respawn_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainGameMode_Respawn()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainGameMode_Respawn_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMainGameMode_RespawnDeadPlayers_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainGameMode_RespawnDeadPlayers_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainGameMode_RespawnDeadPlayers_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainGameMode, nullptr, "RespawnDeadPlayers", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainGameMode_RespawnDeadPlayers_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMainGameMode_RespawnDeadPlayers_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainGameMode_RespawnDeadPlayers()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainGameMode_RespawnDeadPlayers_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMainGameMode_SpawnBot_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainGameMode_SpawnBot_Statics::Function_MetaDataParams[] = {
		{ "Category", "Gamemode" },
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainGameMode_SpawnBot_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainGameMode, nullptr, "SpawnBot", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08080800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainGameMode_SpawnBot_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMainGameMode_SpawnBot_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainGameMode_SpawnBot()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainGameMode_SpawnBot_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMainGameMode_SpawnBotElapsedTimer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainGameMode_SpawnBotElapsedTimer_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainGameMode_SpawnBotElapsedTimer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainGameMode, nullptr, "SpawnBotElapsedTimer", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainGameMode_SpawnBotElapsedTimer_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMainGameMode_SpawnBotElapsedTimer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainGameMode_SpawnBotElapsedTimer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainGameMode_SpawnBotElapsedTimer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMainGameMode_StartWave_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainGameMode_StartWave_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainGameMode_StartWave_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainGameMode, nullptr, "StartWave", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainGameMode_StartWave_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMainGameMode_StartWave_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainGameMode_StartWave()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainGameMode_StartWave_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMainGameMode_NoRegister()
	{
		return AMainGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AMainGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlayersSpawned_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayersSpawned_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PlayersSpawned;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerCharacterClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_PlayerCharacterClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnemyCharacterClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_EnemyCharacterClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AmountBotsToSpawn_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AmountBotsToSpawn;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxWaves_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxWaves;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentWave_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CurrentWave;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeBetweenWaves_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeBetweenWaves;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsPaused_MetaData[];
#endif
		static void NewProp_bIsPaused_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsPaused;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPawnKill_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPawnKill;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPlayerKilled_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPlayerKilled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMainGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_ArmRace,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMainGameMode_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMainGameMode_CheckPlayersAlive, "CheckPlayersAlive" }, // 4120649134
		{ &Z_Construct_UFunction_AMainGameMode_CheckWaveState, "CheckWaveState" }, // 866314778
		{ &Z_Construct_UFunction_AMainGameMode_EndWave, "EndWave" }, // 1164046029
		{ &Z_Construct_UFunction_AMainGameMode_GameOver, "GameOver" }, // 1738019964
		{ &Z_Construct_UFunction_AMainGameMode_PrepareForNextWave, "PrepareForNextWave" }, // 1644593278
		{ &Z_Construct_UFunction_AMainGameMode_Respawn, "Respawn" }, // 1873124492
		{ &Z_Construct_UFunction_AMainGameMode_RespawnDeadPlayers, "RespawnDeadPlayers" }, // 813092481
		{ &Z_Construct_UFunction_AMainGameMode_SpawnBot, "SpawnBot" }, // 1335287380
		{ &Z_Construct_UFunction_AMainGameMode_SpawnBotElapsedTimer, "SpawnBotElapsedTimer" }, // 2028898939
		{ &Z_Construct_UFunction_AMainGameMode_StartWave, "StartWave" }, // 46505812
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainGameMode_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "GameModes/MainGameMode.h" },
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMainGameMode_Statics::NewProp_PlayersSpawned_Inner = { "PlayersSpawned", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainGameMode_Statics::NewProp_PlayersSpawned_MetaData[] = {
		{ "Category", "MainGameMode" },
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AMainGameMode_Statics::NewProp_PlayersSpawned = { "PlayersSpawned", nullptr, (EPropertyFlags)0x0040000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMainGameMode, PlayersSpawned), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AMainGameMode_Statics::NewProp_PlayersSpawned_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMainGameMode_Statics::NewProp_PlayersSpawned_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainGameMode_Statics::NewProp_PlayerCharacterClass_MetaData[] = {
		{ "Category", "MainGameMode" },
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AMainGameMode_Statics::NewProp_PlayerCharacterClass = { "PlayerCharacterClass", nullptr, (EPropertyFlags)0x0044000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMainGameMode, PlayerCharacterClass), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AMainGameMode_Statics::NewProp_PlayerCharacterClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMainGameMode_Statics::NewProp_PlayerCharacterClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainGameMode_Statics::NewProp_EnemyCharacterClass_MetaData[] = {
		{ "Category", "MainGameMode" },
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AMainGameMode_Statics::NewProp_EnemyCharacterClass = { "EnemyCharacterClass", nullptr, (EPropertyFlags)0x0044000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMainGameMode, EnemyCharacterClass), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AMainGameMode_Statics::NewProp_EnemyCharacterClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMainGameMode_Statics::NewProp_EnemyCharacterClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainGameMode_Statics::NewProp_AmountBotsToSpawn_MetaData[] = {
		{ "Category", "MainGameMode" },
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AMainGameMode_Statics::NewProp_AmountBotsToSpawn = { "AmountBotsToSpawn", nullptr, (EPropertyFlags)0x0040000000010001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMainGameMode, AmountBotsToSpawn), METADATA_PARAMS(Z_Construct_UClass_AMainGameMode_Statics::NewProp_AmountBotsToSpawn_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMainGameMode_Statics::NewProp_AmountBotsToSpawn_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainGameMode_Statics::NewProp_MaxWaves_MetaData[] = {
		{ "Category", "MainGameMode" },
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AMainGameMode_Statics::NewProp_MaxWaves = { "MaxWaves", nullptr, (EPropertyFlags)0x0040000000010001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMainGameMode, MaxWaves), METADATA_PARAMS(Z_Construct_UClass_AMainGameMode_Statics::NewProp_MaxWaves_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMainGameMode_Statics::NewProp_MaxWaves_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainGameMode_Statics::NewProp_CurrentWave_MetaData[] = {
		{ "Category", "MainGameMode" },
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AMainGameMode_Statics::NewProp_CurrentWave = { "CurrentWave", nullptr, (EPropertyFlags)0x0040000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMainGameMode, CurrentWave), METADATA_PARAMS(Z_Construct_UClass_AMainGameMode_Statics::NewProp_CurrentWave_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMainGameMode_Statics::NewProp_CurrentWave_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainGameMode_Statics::NewProp_TimeBetweenWaves_MetaData[] = {
		{ "Category", "MainGameMode" },
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMainGameMode_Statics::NewProp_TimeBetweenWaves = { "TimeBetweenWaves", nullptr, (EPropertyFlags)0x0040000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMainGameMode, TimeBetweenWaves), METADATA_PARAMS(Z_Construct_UClass_AMainGameMode_Statics::NewProp_TimeBetweenWaves_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMainGameMode_Statics::NewProp_TimeBetweenWaves_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainGameMode_Statics::NewProp_bIsPaused_MetaData[] = {
		{ "Category", "MainGameMode" },
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	void Z_Construct_UClass_AMainGameMode_Statics::NewProp_bIsPaused_SetBit(void* Obj)
	{
		((AMainGameMode*)Obj)->bIsPaused = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMainGameMode_Statics::NewProp_bIsPaused = { "bIsPaused", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AMainGameMode), &Z_Construct_UClass_AMainGameMode_Statics::NewProp_bIsPaused_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMainGameMode_Statics::NewProp_bIsPaused_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMainGameMode_Statics::NewProp_bIsPaused_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainGameMode_Statics::NewProp_OnPawnKill_MetaData[] = {
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_AMainGameMode_Statics::NewProp_OnPawnKill = { "OnPawnKill", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMainGameMode, OnPawnKill), Z_Construct_UDelegateFunction_ArmRace_OnPawnKill__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_AMainGameMode_Statics::NewProp_OnPawnKill_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMainGameMode_Statics::NewProp_OnPawnKill_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainGameMode_Statics::NewProp_OnPlayerKilled_MetaData[] = {
		{ "ModuleRelativePath", "GameModes/MainGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_AMainGameMode_Statics::NewProp_OnPlayerKilled = { "OnPlayerKilled", nullptr, (EPropertyFlags)0x0010000000080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMainGameMode, OnPlayerKilled), Z_Construct_UDelegateFunction_ArmRace_OnPlayerKilled__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_AMainGameMode_Statics::NewProp_OnPlayerKilled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMainGameMode_Statics::NewProp_OnPlayerKilled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMainGameMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainGameMode_Statics::NewProp_PlayersSpawned_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainGameMode_Statics::NewProp_PlayersSpawned,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainGameMode_Statics::NewProp_PlayerCharacterClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainGameMode_Statics::NewProp_EnemyCharacterClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainGameMode_Statics::NewProp_AmountBotsToSpawn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainGameMode_Statics::NewProp_MaxWaves,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainGameMode_Statics::NewProp_CurrentWave,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainGameMode_Statics::NewProp_TimeBetweenWaves,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainGameMode_Statics::NewProp_bIsPaused,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainGameMode_Statics::NewProp_OnPawnKill,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainGameMode_Statics::NewProp_OnPlayerKilled,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMainGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMainGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMainGameMode_Statics::ClassParams = {
		&AMainGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AMainGameMode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AMainGameMode_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AMainGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMainGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMainGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMainGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMainGameMode, 675161371);
	template<> ARMRACE_API UClass* StaticClass<AMainGameMode>()
	{
		return AMainGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMainGameMode(Z_Construct_UClass_AMainGameMode, &AMainGameMode::StaticClass, TEXT("/Script/ArmRace"), TEXT("AMainGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMainGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
