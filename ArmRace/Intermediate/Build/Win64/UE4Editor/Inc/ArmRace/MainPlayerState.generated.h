// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARMRACE_MainPlayerState_generated_h
#error "MainPlayerState.generated.h already included, missing '#pragma once' in MainPlayerState.h"
#endif
#define ARMRACE_MainPlayerState_generated_h

#define ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_SPARSE_DATA
#define ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAddScore);


#define ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAddScore);


#define ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMainPlayerState(); \
	friend struct Z_Construct_UClass_AMainPlayerState_Statics; \
public: \
	DECLARE_CLASS(AMainPlayerState, APlayerState, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ArmRace"), NO_API) \
	DECLARE_SERIALIZER(AMainPlayerState)


#define ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMainPlayerState(); \
	friend struct Z_Construct_UClass_AMainPlayerState_Statics; \
public: \
	DECLARE_CLASS(AMainPlayerState, APlayerState, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ArmRace"), NO_API) \
	DECLARE_SERIALIZER(AMainPlayerState)


#define ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMainPlayerState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMainPlayerState) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainPlayerState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainPlayerState); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainPlayerState(AMainPlayerState&&); \
	NO_API AMainPlayerState(const AMainPlayerState&); \
public:


#define ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMainPlayerState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainPlayerState(AMainPlayerState&&); \
	NO_API AMainPlayerState(const AMainPlayerState&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainPlayerState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainPlayerState); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMainPlayerState)


#define ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_PRIVATE_PROPERTY_OFFSET
#define ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_12_PROLOG
#define ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_PRIVATE_PROPERTY_OFFSET \
	ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_SPARSE_DATA \
	ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_RPC_WRAPPERS \
	ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_INCLASS \
	ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_PRIVATE_PROPERTY_OFFSET \
	ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_SPARSE_DATA \
	ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_INCLASS_NO_PURE_DECLS \
	ArmRace_Source_ArmRace_GameModes_MainPlayerState_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARMRACE_API UClass* StaticClass<class AMainPlayerState>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ArmRace_Source_ArmRace_GameModes_MainPlayerState_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
