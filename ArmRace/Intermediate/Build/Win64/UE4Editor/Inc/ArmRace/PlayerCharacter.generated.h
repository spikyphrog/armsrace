// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UHealthComponent;
class UDamageType;
class AController;
class AActor;
#ifdef ARMRACE_PlayerCharacter_generated_h
#error "PlayerCharacter.generated.h already included, missing '#pragma once' in PlayerCharacter.h"
#endif
#define ARMRACE_PlayerCharacter_generated_h

#define ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_SPARSE_DATA
#define ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHealthChanged); \
	DECLARE_FUNCTION(execSpawnWeapon); \
	DECLARE_FUNCTION(execReload); \
	DECLARE_FUNCTION(execZoomOut); \
	DECLARE_FUNCTION(execZoomIn); \
	DECLARE_FUNCTION(execStopFire); \
	DECLARE_FUNCTION(execStartFire); \
	DECLARE_FUNCTION(execStandUp); \
	DECLARE_FUNCTION(execPlayerCrouch); \
	DECLARE_FUNCTION(execLookUpRate); \
	DECLARE_FUNCTION(execTurnRate); \
	DECLARE_FUNCTION(execMoveRight); \
	DECLARE_FUNCTION(execMoveForward);


#define ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHealthChanged); \
	DECLARE_FUNCTION(execSpawnWeapon); \
	DECLARE_FUNCTION(execReload); \
	DECLARE_FUNCTION(execZoomOut); \
	DECLARE_FUNCTION(execZoomIn); \
	DECLARE_FUNCTION(execStopFire); \
	DECLARE_FUNCTION(execStartFire); \
	DECLARE_FUNCTION(execStandUp); \
	DECLARE_FUNCTION(execPlayerCrouch); \
	DECLARE_FUNCTION(execLookUpRate); \
	DECLARE_FUNCTION(execTurnRate); \
	DECLARE_FUNCTION(execMoveRight); \
	DECLARE_FUNCTION(execMoveForward);


#define ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerCharacter(); \
	friend struct Z_Construct_UClass_APlayerCharacter_Statics; \
public: \
	DECLARE_CLASS(APlayerCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ArmRace"), NO_API) \
	DECLARE_SERIALIZER(APlayerCharacter) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		CurrentWeapon=NETFIELD_REP_START, \
		PooledWeapons, \
		bDied, \
		MaxLevel, \
		CurrentLevel, \
		NETFIELD_REP_END=CurrentLevel	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerCharacter(); \
	friend struct Z_Construct_UClass_APlayerCharacter_Statics; \
public: \
	DECLARE_CLASS(APlayerCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ArmRace"), NO_API) \
	DECLARE_SERIALIZER(APlayerCharacter) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		CurrentWeapon=NETFIELD_REP_START, \
		PooledWeapons, \
		bDied, \
		MaxLevel, \
		CurrentLevel, \
		NETFIELD_REP_END=CurrentLevel	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerCharacter(APlayerCharacter&&); \
	NO_API APlayerCharacter(const APlayerCharacter&); \
public:


#define ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerCharacter(APlayerCharacter&&); \
	NO_API APlayerCharacter(const APlayerCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerCharacter)


#define ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Camera() { return STRUCT_OFFSET(APlayerCharacter, Camera); } \
	FORCEINLINE static uint32 __PPO__SpringArm() { return STRUCT_OFFSET(APlayerCharacter, SpringArm); } \
	FORCEINLINE static uint32 __PPO__HealthComponent() { return STRUCT_OFFSET(APlayerCharacter, HealthComponent); } \
	FORCEINLINE static uint32 __PPO__TurnRotation() { return STRUCT_OFFSET(APlayerCharacter, TurnRotation); } \
	FORCEINLINE static uint32 __PPO__LookRate() { return STRUCT_OFFSET(APlayerCharacter, LookRate); } \
	FORCEINLINE static uint32 __PPO__bZoomIn() { return STRUCT_OFFSET(APlayerCharacter, bZoomIn); } \
	FORCEINLINE static uint32 __PPO__ZoomedFOV() { return STRUCT_OFFSET(APlayerCharacter, ZoomedFOV); } \
	FORCEINLINE static uint32 __PPO__DefaultFOV() { return STRUCT_OFFSET(APlayerCharacter, DefaultFOV); } \
	FORCEINLINE static uint32 __PPO__ZoomCheckTimerHandle() { return STRUCT_OFFSET(APlayerCharacter, ZoomCheckTimerHandle); } \
	FORCEINLINE static uint32 __PPO__TimerDeltaTime() { return STRUCT_OFFSET(APlayerCharacter, TimerDeltaTime); } \
	FORCEINLINE static uint32 __PPO__ZoomInterpolationSpeed() { return STRUCT_OFFSET(APlayerCharacter, ZoomInterpolationSpeed); } \
	FORCEINLINE static uint32 __PPO__CurrentWeapon() { return STRUCT_OFFSET(APlayerCharacter, CurrentWeapon); } \
	FORCEINLINE static uint32 __PPO__PooledWeapons() { return STRUCT_OFFSET(APlayerCharacter, PooledWeapons); } \
	FORCEINLINE static uint32 __PPO__AKWeapon() { return STRUCT_OFFSET(APlayerCharacter, AKWeapon); } \
	FORCEINLINE static uint32 __PPO__AKUWeapon() { return STRUCT_OFFSET(APlayerCharacter, AKUWeapon); } \
	FORCEINLINE static uint32 __PPO__ARWeapon() { return STRUCT_OFFSET(APlayerCharacter, ARWeapon); } \
	FORCEINLINE static uint32 __PPO__ARUWeapon() { return STRUCT_OFFSET(APlayerCharacter, ARUWeapon); } \
	FORCEINLINE static uint32 __PPO__WeaponSocketName() { return STRUCT_OFFSET(APlayerCharacter, WeaponSocketName); } \
	FORCEINLINE static uint32 __PPO__bDied() { return STRUCT_OFFSET(APlayerCharacter, bDied); }


#define ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_12_PROLOG
#define ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_SPARSE_DATA \
	ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_RPC_WRAPPERS \
	ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_INCLASS \
	ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_SPARSE_DATA \
	ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_INCLASS_NO_PURE_DECLS \
	ArmRace_Source_ArmRace_Characters_PlayerCharacter_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARMRACE_API UClass* StaticClass<class APlayerCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ArmRace_Source_ArmRace_Characters_PlayerCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
