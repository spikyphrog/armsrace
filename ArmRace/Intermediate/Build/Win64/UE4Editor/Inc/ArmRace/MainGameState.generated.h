// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EWaveState : uint8;
#ifdef ARMRACE_MainGameState_generated_h
#error "MainGameState.generated.h already included, missing '#pragma once' in MainGameState.h"
#endif
#define ARMRACE_MainGameState_generated_h

#define ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_SPARSE_DATA
#define ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnRep_WaveState); \
	DECLARE_FUNCTION(execSetWaveState);


#define ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnRep_WaveState); \
	DECLARE_FUNCTION(execSetWaveState);


#define ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_EVENT_PARMS \
	struct MainGameState_eventWaveStateChanged_Parms \
	{ \
		EWaveState NewState; \
		EWaveState OldState; \
	};


#define ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_CALLBACK_WRAPPERS
#define ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMainGameState(); \
	friend struct Z_Construct_UClass_AMainGameState_Statics; \
public: \
	DECLARE_CLASS(AMainGameState, AGameStateBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ArmRace"), NO_API) \
	DECLARE_SERIALIZER(AMainGameState) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		WaveState=NETFIELD_REP_START, \
		NETFIELD_REP_END=WaveState	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_INCLASS \
private: \
	static void StaticRegisterNativesAMainGameState(); \
	friend struct Z_Construct_UClass_AMainGameState_Statics; \
public: \
	DECLARE_CLASS(AMainGameState, AGameStateBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ArmRace"), NO_API) \
	DECLARE_SERIALIZER(AMainGameState) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		WaveState=NETFIELD_REP_START, \
		NETFIELD_REP_END=WaveState	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMainGameState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMainGameState) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainGameState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainGameState); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainGameState(AMainGameState&&); \
	NO_API AMainGameState(const AMainGameState&); \
public:


#define ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMainGameState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainGameState(AMainGameState&&); \
	NO_API AMainGameState(const AMainGameState&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainGameState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainGameState); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMainGameState)


#define ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_PRIVATE_PROPERTY_OFFSET
#define ArmRace_Source_ArmRace_GameModes_MainGameState_h_29_PROLOG \
	ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_EVENT_PARMS


#define ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_PRIVATE_PROPERTY_OFFSET \
	ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_SPARSE_DATA \
	ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_RPC_WRAPPERS \
	ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_CALLBACK_WRAPPERS \
	ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_INCLASS \
	ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_PRIVATE_PROPERTY_OFFSET \
	ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_SPARSE_DATA \
	ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_CALLBACK_WRAPPERS \
	ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_INCLASS_NO_PURE_DECLS \
	ArmRace_Source_ArmRace_GameModes_MainGameState_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARMRACE_API UClass* StaticClass<class AMainGameState>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ArmRace_Source_ArmRace_GameModes_MainGameState_h


#define FOREACH_ENUM_EWAVESTATE(op) \
	op(EWaveState::WaitingToStart) \
	op(EWaveState::PreparingNextWave) \
	op(EWaveState::WaveInProgress) \
	op(EWaveState::WaitingToComplete) \
	op(EWaveState::WaveComplete) \
	op(EWaveState::GameOver) 

enum class EWaveState : uint8;
template<> ARMRACE_API UEnum* StaticEnum<EWaveState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
