// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARMRACE_Weapon_generated_h
#error "Weapon.generated.h already included, missing '#pragma once' in Weapon.h"
#endif
#define ARMRACE_Weapon_generated_h

#define ArmRace_Source_ArmRace_Actors_Weapon_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHitScanTrace_Statics; \
	ARMRACE_API static class UScriptStruct* StaticStruct();


template<> ARMRACE_API UScriptStruct* StaticStruct<struct FHitScanTrace>();

#define ArmRace_Source_ArmRace_Actors_Weapon_h_9_DELEGATE \
struct _Script_ArmRace_eventOnWeaponReload_Parms \
{ \
	bool IsReloading; \
}; \
static inline void FOnWeaponReload_DelegateWrapper(const FMulticastScriptDelegate& OnWeaponReload, bool IsReloading) \
{ \
	_Script_ArmRace_eventOnWeaponReload_Parms Parms; \
	Parms.IsReloading=IsReloading ? true : false; \
	OnWeaponReload.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define ArmRace_Source_ArmRace_Actors_Weapon_h_27_SPARSE_DATA
#define ArmRace_Source_ArmRace_Actors_Weapon_h_27_RPC_WRAPPERS \
	virtual bool ServerReload_Validate(); \
	virtual void ServerReload_Implementation(); \
	virtual bool ServerFire_Validate(); \
	virtual void ServerFire_Implementation(); \
 \
	DECLARE_FUNCTION(execCheckReloading); \
	DECLARE_FUNCTION(execServerReload); \
	DECLARE_FUNCTION(execServerFire); \
	DECLARE_FUNCTION(execOnRep_HitScanTrace);


#define ArmRace_Source_ArmRace_Actors_Weapon_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual bool ServerReload_Validate(); \
	virtual void ServerReload_Implementation(); \
	virtual bool ServerFire_Validate(); \
	virtual void ServerFire_Implementation(); \
 \
	DECLARE_FUNCTION(execCheckReloading); \
	DECLARE_FUNCTION(execServerReload); \
	DECLARE_FUNCTION(execServerFire); \
	DECLARE_FUNCTION(execOnRep_HitScanTrace);


#define ArmRace_Source_ArmRace_Actors_Weapon_h_27_EVENT_PARMS
#define ArmRace_Source_ArmRace_Actors_Weapon_h_27_CALLBACK_WRAPPERS
#define ArmRace_Source_ArmRace_Actors_Weapon_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWeapon(); \
	friend struct Z_Construct_UClass_AWeapon_Statics; \
public: \
	DECLARE_CLASS(AWeapon, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ArmRace"), NO_API) \
	DECLARE_SERIALIZER(AWeapon) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		CurrentClipCapacity=NETFIELD_REP_START, \
		HitScanTrace, \
		NETFIELD_REP_END=HitScanTrace	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define ArmRace_Source_ArmRace_Actors_Weapon_h_27_INCLASS \
private: \
	static void StaticRegisterNativesAWeapon(); \
	friend struct Z_Construct_UClass_AWeapon_Statics; \
public: \
	DECLARE_CLASS(AWeapon, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ArmRace"), NO_API) \
	DECLARE_SERIALIZER(AWeapon) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		CurrentClipCapacity=NETFIELD_REP_START, \
		HitScanTrace, \
		NETFIELD_REP_END=HitScanTrace	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define ArmRace_Source_ArmRace_Actors_Weapon_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWeapon(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWeapon) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeapon); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeapon(AWeapon&&); \
	NO_API AWeapon(const AWeapon&); \
public:


#define ArmRace_Source_ArmRace_Actors_Weapon_h_27_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeapon(AWeapon&&); \
	NO_API AWeapon(const AWeapon&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeapon); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWeapon)


#define ArmRace_Source_ArmRace_Actors_Weapon_h_27_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MeshComponent() { return STRUCT_OFFSET(AWeapon, MeshComponent); } \
	FORCEINLINE static uint32 __PPO__DamageTypeClass() { return STRUCT_OFFSET(AWeapon, DamageTypeClass); } \
	FORCEINLINE static uint32 __PPO__MuzzleEffect() { return STRUCT_OFFSET(AWeapon, MuzzleEffect); } \
	FORCEINLINE static uint32 __PPO__DefaultImpactEffect() { return STRUCT_OFFSET(AWeapon, DefaultImpactEffect); } \
	FORCEINLINE static uint32 __PPO__FleshImpactEffect() { return STRUCT_OFFSET(AWeapon, FleshImpactEffect); } \
	FORCEINLINE static uint32 __PPO__BaseDamage() { return STRUCT_OFFSET(AWeapon, BaseDamage); } \
	FORCEINLINE static uint32 __PPO__RateOfFire() { return STRUCT_OFFSET(AWeapon, RateOfFire); } \
	FORCEINLINE static uint32 __PPO__TimeToReload() { return STRUCT_OFFSET(AWeapon, TimeToReload); } \
	FORCEINLINE static uint32 __PPO__MaxClipCapacity() { return STRUCT_OFFSET(AWeapon, MaxClipCapacity); } \
	FORCEINLINE static uint32 __PPO__CurrentClipCapacity() { return STRUCT_OFFSET(AWeapon, CurrentClipCapacity); } \
	FORCEINLINE static uint32 __PPO__MuzzleSocket() { return STRUCT_OFFSET(AWeapon, MuzzleSocket); } \
	FORCEINLINE static uint32 __PPO__TimerHandle_TimeBetweenShots() { return STRUCT_OFFSET(AWeapon, TimerHandle_TimeBetweenShots); } \
	FORCEINLINE static uint32 __PPO__TimerHandle_TimeToReload() { return STRUCT_OFFSET(AWeapon, TimerHandle_TimeToReload); } \
	FORCEINLINE static uint32 __PPO__TimeBetweenShots() { return STRUCT_OFFSET(AWeapon, TimeBetweenShots); } \
	FORCEINLINE static uint32 __PPO__LastTimeShot() { return STRUCT_OFFSET(AWeapon, LastTimeShot); } \
	FORCEINLINE static uint32 __PPO__HitScanTrace() { return STRUCT_OFFSET(AWeapon, HitScanTrace); }


#define ArmRace_Source_ArmRace_Actors_Weapon_h_24_PROLOG \
	ArmRace_Source_ArmRace_Actors_Weapon_h_27_EVENT_PARMS


#define ArmRace_Source_ArmRace_Actors_Weapon_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ArmRace_Source_ArmRace_Actors_Weapon_h_27_PRIVATE_PROPERTY_OFFSET \
	ArmRace_Source_ArmRace_Actors_Weapon_h_27_SPARSE_DATA \
	ArmRace_Source_ArmRace_Actors_Weapon_h_27_RPC_WRAPPERS \
	ArmRace_Source_ArmRace_Actors_Weapon_h_27_CALLBACK_WRAPPERS \
	ArmRace_Source_ArmRace_Actors_Weapon_h_27_INCLASS \
	ArmRace_Source_ArmRace_Actors_Weapon_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ArmRace_Source_ArmRace_Actors_Weapon_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ArmRace_Source_ArmRace_Actors_Weapon_h_27_PRIVATE_PROPERTY_OFFSET \
	ArmRace_Source_ArmRace_Actors_Weapon_h_27_SPARSE_DATA \
	ArmRace_Source_ArmRace_Actors_Weapon_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	ArmRace_Source_ArmRace_Actors_Weapon_h_27_CALLBACK_WRAPPERS \
	ArmRace_Source_ArmRace_Actors_Weapon_h_27_INCLASS_NO_PURE_DECLS \
	ArmRace_Source_ArmRace_Actors_Weapon_h_27_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARMRACE_API UClass* StaticClass<class AWeapon>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ArmRace_Source_ArmRace_Actors_Weapon_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
