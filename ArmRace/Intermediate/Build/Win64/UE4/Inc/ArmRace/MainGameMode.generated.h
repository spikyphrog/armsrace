// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class AController;
#ifdef ARMRACE_MainGameMode_generated_h
#error "MainGameMode.generated.h already included, missing '#pragma once' in MainGameMode.h"
#endif
#define ARMRACE_MainGameMode_generated_h

#define ArmRace_Source_ArmRace_GameModes_MainGameMode_h_12_DELEGATE \
static inline void FOnPlayerKilled_DelegateWrapper(const FMulticastScriptDelegate& OnPlayerKilled) \
{ \
	OnPlayerKilled.ProcessMulticastDelegate<UObject>(NULL); \
}


#define ArmRace_Source_ArmRace_GameModes_MainGameMode_h_11_DELEGATE \
struct _Script_ArmRace_eventOnPawnKill_Parms \
{ \
	AActor* VictimActor; \
	AActor* KillerActor; \
	AController* KillerController; \
}; \
static inline void FOnPawnKill_DelegateWrapper(const FMulticastScriptDelegate& OnPawnKill, AActor* VictimActor, AActor* KillerActor, AController* KillerController) \
{ \
	_Script_ArmRace_eventOnPawnKill_Parms Parms; \
	Parms.VictimActor=VictimActor; \
	Parms.KillerActor=KillerActor; \
	Parms.KillerController=KillerController; \
	OnPawnKill.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_SPARSE_DATA
#define ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRespawn); \
	DECLARE_FUNCTION(execRespawnDeadPlayers); \
	DECLARE_FUNCTION(execCheckPlayersAlive); \
	DECLARE_FUNCTION(execCheckWaveState); \
	DECLARE_FUNCTION(execPrepareForNextWave); \
	DECLARE_FUNCTION(execGameOver); \
	DECLARE_FUNCTION(execEndWave); \
	DECLARE_FUNCTION(execStartWave); \
	DECLARE_FUNCTION(execSpawnBotElapsedTimer);


#define ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRespawn); \
	DECLARE_FUNCTION(execRespawnDeadPlayers); \
	DECLARE_FUNCTION(execCheckPlayersAlive); \
	DECLARE_FUNCTION(execCheckWaveState); \
	DECLARE_FUNCTION(execPrepareForNextWave); \
	DECLARE_FUNCTION(execGameOver); \
	DECLARE_FUNCTION(execEndWave); \
	DECLARE_FUNCTION(execStartWave); \
	DECLARE_FUNCTION(execSpawnBotElapsedTimer);


#define ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_EVENT_PARMS
#define ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_CALLBACK_WRAPPERS
#define ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMainGameMode(); \
	friend struct Z_Construct_UClass_AMainGameMode_Statics; \
public: \
	DECLARE_CLASS(AMainGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ArmRace"), NO_API) \
	DECLARE_SERIALIZER(AMainGameMode)


#define ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_INCLASS \
private: \
	static void StaticRegisterNativesAMainGameMode(); \
	friend struct Z_Construct_UClass_AMainGameMode_Statics; \
public: \
	DECLARE_CLASS(AMainGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ArmRace"), NO_API) \
	DECLARE_SERIALIZER(AMainGameMode)


#define ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMainGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMainGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainGameMode(AMainGameMode&&); \
	NO_API AMainGameMode(const AMainGameMode&); \
public:


#define ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainGameMode(AMainGameMode&&); \
	NO_API AMainGameMode(const AMainGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMainGameMode)


#define ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PlayersSpawned() { return STRUCT_OFFSET(AMainGameMode, PlayersSpawned); } \
	FORCEINLINE static uint32 __PPO__PlayerCharacterClass() { return STRUCT_OFFSET(AMainGameMode, PlayerCharacterClass); } \
	FORCEINLINE static uint32 __PPO__EnemyCharacterClass() { return STRUCT_OFFSET(AMainGameMode, EnemyCharacterClass); } \
	FORCEINLINE static uint32 __PPO__AmountBotsToSpawn() { return STRUCT_OFFSET(AMainGameMode, AmountBotsToSpawn); } \
	FORCEINLINE static uint32 __PPO__MaxWaves() { return STRUCT_OFFSET(AMainGameMode, MaxWaves); } \
	FORCEINLINE static uint32 __PPO__CurrentWave() { return STRUCT_OFFSET(AMainGameMode, CurrentWave); } \
	FORCEINLINE static uint32 __PPO__TimeBetweenWaves() { return STRUCT_OFFSET(AMainGameMode, TimeBetweenWaves); }


#define ArmRace_Source_ArmRace_GameModes_MainGameMode_h_16_PROLOG \
	ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_EVENT_PARMS


#define ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_PRIVATE_PROPERTY_OFFSET \
	ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_SPARSE_DATA \
	ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_RPC_WRAPPERS \
	ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_CALLBACK_WRAPPERS \
	ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_INCLASS \
	ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_PRIVATE_PROPERTY_OFFSET \
	ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_SPARSE_DATA \
	ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_CALLBACK_WRAPPERS \
	ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_INCLASS_NO_PURE_DECLS \
	ArmRace_Source_ArmRace_GameModes_MainGameMode_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARMRACE_API UClass* StaticClass<class AMainGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ArmRace_Source_ArmRace_GameModes_MainGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
