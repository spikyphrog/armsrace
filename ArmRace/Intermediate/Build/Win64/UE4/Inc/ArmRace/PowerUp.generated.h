// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UHealthComponent;
class UDamageType;
class AController;
class AActor;
#ifdef ARMRACE_PowerUp_generated_h
#error "PowerUp.generated.h already included, missing '#pragma once' in PowerUp.h"
#endif
#define ARMRACE_PowerUp_generated_h

#define ArmRace_Source_ArmRace_Actors_PowerUp_h_13_SPARSE_DATA
#define ArmRace_Source_ArmRace_Actors_PowerUp_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHealthChanged); \
	DECLARE_FUNCTION(execOnRep_Exploded);


#define ArmRace_Source_ArmRace_Actors_PowerUp_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHealthChanged); \
	DECLARE_FUNCTION(execOnRep_Exploded);


#define ArmRace_Source_ArmRace_Actors_PowerUp_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPowerUp(); \
	friend struct Z_Construct_UClass_APowerUp_Statics; \
public: \
	DECLARE_CLASS(APowerUp, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ArmRace"), NO_API) \
	DECLARE_SERIALIZER(APowerUp) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		bDied=NETFIELD_REP_START, \
		NETFIELD_REP_END=bDied	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define ArmRace_Source_ArmRace_Actors_PowerUp_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAPowerUp(); \
	friend struct Z_Construct_UClass_APowerUp_Statics; \
public: \
	DECLARE_CLASS(APowerUp, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ArmRace"), NO_API) \
	DECLARE_SERIALIZER(APowerUp) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		bDied=NETFIELD_REP_START, \
		NETFIELD_REP_END=bDied	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define ArmRace_Source_ArmRace_Actors_PowerUp_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APowerUp(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APowerUp) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APowerUp); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APowerUp); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APowerUp(APowerUp&&); \
	NO_API APowerUp(const APowerUp&); \
public:


#define ArmRace_Source_ArmRace_Actors_PowerUp_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APowerUp(APowerUp&&); \
	NO_API APowerUp(const APowerUp&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APowerUp); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APowerUp); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APowerUp)


#define ArmRace_Source_ArmRace_Actors_PowerUp_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__HealthComponent() { return STRUCT_OFFSET(APowerUp, HealthComponent); } \
	FORCEINLINE static uint32 __PPO__StaticMeshComponent() { return STRUCT_OFFSET(APowerUp, StaticMeshComponent); } \
	FORCEINLINE static uint32 __PPO__ExplosionEffect() { return STRUCT_OFFSET(APowerUp, ExplosionEffect); } \
	FORCEINLINE static uint32 __PPO__RadialForceComponent() { return STRUCT_OFFSET(APowerUp, RadialForceComponent); } \
	FORCEINLINE static uint32 __PPO__ExplosionImpulse() { return STRUCT_OFFSET(APowerUp, ExplosionImpulse); }


#define ArmRace_Source_ArmRace_Actors_PowerUp_h_10_PROLOG
#define ArmRace_Source_ArmRace_Actors_PowerUp_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ArmRace_Source_ArmRace_Actors_PowerUp_h_13_PRIVATE_PROPERTY_OFFSET \
	ArmRace_Source_ArmRace_Actors_PowerUp_h_13_SPARSE_DATA \
	ArmRace_Source_ArmRace_Actors_PowerUp_h_13_RPC_WRAPPERS \
	ArmRace_Source_ArmRace_Actors_PowerUp_h_13_INCLASS \
	ArmRace_Source_ArmRace_Actors_PowerUp_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ArmRace_Source_ArmRace_Actors_PowerUp_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ArmRace_Source_ArmRace_Actors_PowerUp_h_13_PRIVATE_PROPERTY_OFFSET \
	ArmRace_Source_ArmRace_Actors_PowerUp_h_13_SPARSE_DATA \
	ArmRace_Source_ArmRace_Actors_PowerUp_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	ArmRace_Source_ArmRace_Actors_PowerUp_h_13_INCLASS_NO_PURE_DECLS \
	ArmRace_Source_ArmRace_Actors_PowerUp_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARMRACE_API UClass* StaticClass<class APowerUp>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ArmRace_Source_ArmRace_Actors_PowerUp_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
