// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UHealthComponent;
class UDamageType;
class AController;
class AActor;
#ifdef ARMRACE_HealthComponent_generated_h
#error "HealthComponent.generated.h already included, missing '#pragma once' in HealthComponent.h"
#endif
#define ARMRACE_HealthComponent_generated_h

#define ArmRace_Source_ArmRace_Components_HealthComponent_h_10_DELEGATE \
struct _Script_ArmRace_eventOnHealthChanged_Parms \
{ \
	UHealthComponent* HealthComp; \
	float CurrentHealth; \
	float Damage; \
	const UDamageType* DamageType; \
	AController* InstigatedBy; \
	AActor* DamageCauser; \
}; \
static inline void FOnHealthChanged_DelegateWrapper(const FMulticastScriptDelegate& OnHealthChanged, UHealthComponent* HealthComp, float CurrentHealth, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser) \
{ \
	_Script_ArmRace_eventOnHealthChanged_Parms Parms; \
	Parms.HealthComp=HealthComp; \
	Parms.CurrentHealth=CurrentHealth; \
	Parms.Damage=Damage; \
	Parms.DamageType=DamageType; \
	Parms.InstigatedBy=InstigatedBy; \
	Parms.DamageCauser=DamageCauser; \
	OnHealthChanged.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define ArmRace_Source_ArmRace_Components_HealthComponent_h_15_SPARSE_DATA
#define ArmRace_Source_ArmRace_Components_HealthComponent_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHandleDamage); \
	DECLARE_FUNCTION(execGetHealth);


#define ArmRace_Source_ArmRace_Components_HealthComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHandleDamage); \
	DECLARE_FUNCTION(execGetHealth);


#define ArmRace_Source_ArmRace_Components_HealthComponent_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHealthComponent(); \
	friend struct Z_Construct_UClass_UHealthComponent_Statics; \
public: \
	DECLARE_CLASS(UHealthComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ArmRace"), NO_API) \
	DECLARE_SERIALIZER(UHealthComponent) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		CurrentHealth=NETFIELD_REP_START, \
		NETFIELD_REP_END=CurrentHealth	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define ArmRace_Source_ArmRace_Components_HealthComponent_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUHealthComponent(); \
	friend struct Z_Construct_UClass_UHealthComponent_Statics; \
public: \
	DECLARE_CLASS(UHealthComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ArmRace"), NO_API) \
	DECLARE_SERIALIZER(UHealthComponent) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		CurrentHealth=NETFIELD_REP_START, \
		NETFIELD_REP_END=CurrentHealth	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define ArmRace_Source_ArmRace_Components_HealthComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHealthComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHealthComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHealthComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHealthComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHealthComponent(UHealthComponent&&); \
	NO_API UHealthComponent(const UHealthComponent&); \
public:


#define ArmRace_Source_ArmRace_Components_HealthComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHealthComponent(UHealthComponent&&); \
	NO_API UHealthComponent(const UHealthComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHealthComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHealthComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UHealthComponent)


#define ArmRace_Source_ArmRace_Components_HealthComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MaxHealth() { return STRUCT_OFFSET(UHealthComponent, MaxHealth); } \
	FORCEINLINE static uint32 __PPO__CurrentHealth() { return STRUCT_OFFSET(UHealthComponent, CurrentHealth); } \
	FORCEINLINE static uint32 __PPO__bHasAwardedPoint() { return STRUCT_OFFSET(UHealthComponent, bHasAwardedPoint); }


#define ArmRace_Source_ArmRace_Components_HealthComponent_h_12_PROLOG
#define ArmRace_Source_ArmRace_Components_HealthComponent_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ArmRace_Source_ArmRace_Components_HealthComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	ArmRace_Source_ArmRace_Components_HealthComponent_h_15_SPARSE_DATA \
	ArmRace_Source_ArmRace_Components_HealthComponent_h_15_RPC_WRAPPERS \
	ArmRace_Source_ArmRace_Components_HealthComponent_h_15_INCLASS \
	ArmRace_Source_ArmRace_Components_HealthComponent_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ArmRace_Source_ArmRace_Components_HealthComponent_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ArmRace_Source_ArmRace_Components_HealthComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	ArmRace_Source_ArmRace_Components_HealthComponent_h_15_SPARSE_DATA \
	ArmRace_Source_ArmRace_Components_HealthComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	ArmRace_Source_ArmRace_Components_HealthComponent_h_15_INCLASS_NO_PURE_DECLS \
	ArmRace_Source_ArmRace_Components_HealthComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARMRACE_API UClass* StaticClass<class UHealthComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ArmRace_Source_ArmRace_Components_HealthComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
